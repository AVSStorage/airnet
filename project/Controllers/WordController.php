<?php

require_once('/code/Models/WordModel.php');

class WordController
{

    private $dictionary;
    public function __construct()
    {
        $this->dictionary = new WordModel();
    }

    public function add($word, $definitions, $pronunciations = null) {



        $checkIfWordExists = $this->dictionary->getWordByName($word);
        if (!empty($checkIfWordExists)) {
            return json_encode(['error' => true, 'message' => 'Word is already exists']);
        }


        try {
            $this->dictionary->addWord($word);
            $word_id = $this->dictionary->getWordByName($word)['id'];
            $this->dictionary->addDefinitions($definitions, $word_id);
            if ($pronunciations) {
                $this->dictionary->addPronunciations($pronunciations, $word_id);
            }

        } catch (PDOException $exception) {
            return json_encode(['error' => true, 'message' => $exception->getMessage()]);
        }


        return  json_encode(['error' => false, 'message' => 'OK']);


    }

    public function getAllExistingData($word) {
        return json_encode(['definitions' => array_map(function ($item) {
            return $item['definition'];
        }, $this->dictionary->getDefinitions($word)),
            'pronunciations' => $this->dictionary->getPronunciations($word)]);
    }

    public function get($word) {
        return json_encode($this->dictionary->getNameByFilter($word));
    }
}