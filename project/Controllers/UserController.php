<?php


require('/code/Models/UserModel.php');
class UserController
{
 private $user;
 public function __construct()
 {
     $this->user = new UserModel();
 }

    /**
     * @param string $login
     * @param string $password
     * @return string
     */

 public function register($login, $password) {

     try {
         $this->user->addUser($login, $password);
     } catch (PDOException $exception) {
         http_response_code(403);
         return json_encode(['error' => true, 'message' => $exception->getMessage()]);
     }

     return json_encode(['error' => false, 'message' => 'OK']);

 }

    /**
     * @param string $login
     * @param string $password
     * @return string
     */

 public function login($login, $password) {



     $result = $this->user->getUserByLogin($login);

     if (!$result){
         http_response_code(403);
         return json_encode(['error' => true, 'message' => "User not found"]);
     }

     if (!$this->user->compareUserPassword($password, $result['password'])) {
         http_response_code(403);
         return json_encode(['error' => true, 'message' => "Not correct password. Please enter again"]);
     }


     $_SESSION['user_id'] = $result['id'];

     return json_encode(['error' => false, 'message' => "OK"]);

 }

}