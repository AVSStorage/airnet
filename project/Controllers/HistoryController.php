<?php
require_once('/code/Models/HistoryModel.php');
require_once('/code/Models/WordModel.php');

class HistoryController
{

    private $history;
    private $dictionary;
    public function __construct()
    {
        $this->history = new HistoryModel();
        $this->dictionary = new WordModel();
    }

    /**
     * @param string|int $user_id
     * @param string $word
     * @return string
     */

    public function add($user_id, $word) {
        $word_id =  $this->dictionary->getWordByName($word)['id'];
        try {
            $this->history->addHistory($user_id, $word_id);
        } catch (PDOException $exception) {
            return json_encode(['error' => true, 'message' => $exception->getMessage()]);
        }

        return json_encode(['error' => false, 'message' => 'OK']);
    }

    /**
     * @param $user_id
     * @return string
     */
    public function get($user_id) {

    
        $words = $this->history->getWordName($user_id);
        $definitions = $this->history->getWordDefinitions($user_id);
      
      
        $pronunciations = $this->history->getWordPronunciations($user_id);

        return json_encode(['words' => $words, 'definitions' => $definitions, 'pronunciations' => $pronunciations]);
    }
}
