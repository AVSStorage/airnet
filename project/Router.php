<?php


class Router
{

    private static $routes = array();


    private function __construct() {}
    private function __clone() {}


    /**
     * @param string $pattern
     * @param callable $callback
     */
    public static function route($pattern, $callback)
    {
        $pattern = '/^' . str_replace('/', '\/', $pattern) . '$/';
        self::$routes[$pattern] = $callback;
    }

    /**
     * @param array $fields
     * @return bool
     */

    public static function checkHasPostData($fields) {
        return count(array_intersect($fields, array_keys($_POST))) >= count($fields);
    }


    /**
     * @param string $url
     * @return mixed
     */
    public static function execute($url)
    {

        foreach (self::$routes as $pattern => $callback)
        {

            if (preg_match($pattern, $url, $params)) // сравнение идет через регулярное выражение
            {

                array_shift($params);
                return call_user_func_array($callback, array_values($params));
            }
        }
    }
}

?>