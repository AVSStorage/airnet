import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Home from './components/Home.vue'
import Dashboard from "./components/Dashboard";
import './assets/css/main.css';
import Word from "./components/Word";

Vue.config.productionTip = false


const routes = [
  { path: '/', component: Home },
  {path: '/dashboard', component: Dashboard},
  {path: '/dictionary/:word', component: Word, name: 'word'}
]

Vue.use(VueRouter)
// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  mode: 'history',
  routes // short for `routes: routes`
})



new Vue({
  router,
  render: h => h(App)

}).$mount('#app')
