<?php

class APIDataMapper {
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getLexicalEntries() {
        return array_shift($this->data['results'])['lexicalEntries'];
    }

    /**
     * @param array $definitionsArray
     * @return array
     */
    public function getDefinitionsData(array $definitionsArray) {
        $result = [];
        foreach ($definitionsArray as $definitionArray) {
            if ($definitionArray['definitions']) {
                $result[] = array_shift($definitionArray['definitions']);
            }
        }
        return $result;
    }

    /**
     * @param array $result
     * @return array
     */
    public function getPronunciations(array $result)  {

        $pronunciations = [];
        foreach ($result as $word) {
            list($dialect_info, $audio_info) = $word['entries'][0]['pronunciations'];

            $newPair = [ 'dialects' => array_shift($dialect_info['dialects']), 'audioFile' => $audio_info['audioFile']];


            // TODO save files to DISK

            if (!in_array($newPair, $pronunciations)) {
                $pronunciations[] =   $newPair;
            }
        }

        return $pronunciations;
    }

    /**
     *
     * @return array
     */
    public function getTransformedData() {
        $result = $this->getLexicalEntries();
        return ['definitions' => $this->getDefinitions($result), 'pronunciations' => $this->getPronunciations($result)];
    }

    /**
     * @param array $result
     * @return array
     */
    public  function getDefinitions(array $result) {

        $definitions = [];
        foreach ($result as $word) {
            $item = $word['entries'][0]['senses'];

            $definitions =   array_merge($definitions, $this->getDefinitionsData($item));
        }

        return $definitions;
    }


}

?>