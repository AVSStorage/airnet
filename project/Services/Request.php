<?php

require('APIDataMapper.php');
class Request
{
   private $url = "https://od-api.oxforddictionaries.com/api/v2/entries/en-us/";

   private $api_params = '?fields=definitions%2Cpronunciations&strictMatch=true';

    /**
     * @return array
     * @throws ErrorException
     */
   private function getHeaders() {
       if (!(getenv('APP_ID') || getenv('APP_KEY'))){
           throw new ErrorException('Local environment variables not defined');
       }
       return ['app_id' => getenv('APP_ID'), 'app_key' => getenv('APP_KEY')];
   }

    /**
     * @param array $headers
     * @param string $url
     * @return string
     * @throws Exception
     */
   public function getRequest($headers, $url) {
       $curl = curl_init($url);
       curl_setopt($curl, CURLOPT_URL,$url);
       curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

       curl_setopt($curl, CURLOPT_FAILONERROR, true);
       curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
       //for debug only!
       curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
       curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

       $resp = curl_exec($curl);
       if (curl_errno($curl)) {
           $error_msg = curl_error($curl);
       }
       curl_close($curl);

       if (isset($error_msg)) {
           throw new Exception('API error');
       }


       return json_decode($resp, true);
   }

    /**
     * @param string $word
     * @return array|string
     */
   public  function  getWord($word) {


       try {
           $headers = $this->getHeaders();
           $url = $this->url . $word . $this->api_params;
           $args = [];
           array_walk(
               $headers,
               function ($item, $key) use (&$args) {
                   $args[] = $key .": " . $item;
               }
           );
           return $this->getRequest($args, $url);
       } catch (Exception $exception) {
           return ['error' => true, 'message' => $exception->getMessage() ];
       }

    }

}