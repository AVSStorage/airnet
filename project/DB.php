<?php


class DB
{

    /**
     * @var PDO
     */
    public $pdo;
    public function __construct()
    {

        $this->connect();
    }

    private function connect() {
        $dsn = "sqlite:".__DIR__."/database.db";
        try {
            $this->pdo = new PDO($dsn);
            $this->pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $this->pdo->setAttribute( PDO::ATTR_EMULATE_PREPARES, false);
        } catch (PDOException $e) {
            die('Подключение не удалось: ' . $e->getMessage());
        }
    }

    /**
     * @param $query string
     */

    public function exec(string $query) {
        $this->pdo->exec($query);
    }

    /**
     * @param string $query
     * @return array
     */
    public function fetchAll(string $query) {
        $sth = $this->pdo->prepare($query);
        $sth->execute();

        return $sth->fetchAll();
    }

    /**
     * @param string $query
     * @return array
     */
    public function fetch($query) {
        $sth = $this->pdo->prepare($query);
        $sth->execute();

        return $sth->fetch();
    }
}