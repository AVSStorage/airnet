<?php
require('/code/DB.php');

class WordModel
{
    private $db;
    public function __construct()
    {
        $this->db = new DB();
    }

    /**
     * @param string $name
     * @return array
     */
    public function getWordByName($name) {
        return $this->db->fetch("SELECT * FROM words WHERE name='{$name}'");
    }

    /**
     * @param string $word
     *
     */
    public function addWord($word){
        $this->db->exec("INSERT INTO words VALUES (NULL, '{$word}')");
    }

    /**
     * @param array $definitions
     * @param string $word
     */
    public function addDefinitions($definitions, $word) {
        foreach ($definitions as $definition) {
            $this->db->exec("INSERT INTO definitions VALUES (NULL, '{$definition}', {$word})");
        }
    }

    /**
     * @param array $pronunciations
     * @param string $word
     */

    public function addPronunciations($pronunciations, $word) {
        foreach ($pronunciations as $pronunciation) {
            $this->db->exec("INSERT INTO pronunciations VALUES (NULL, '{$pronunciation['dialects']}', '{$pronunciation['audioFile']}', {$word})");
        }
    }

    /**
     * @param string $word
     * @return array
     */

    public function getPronunciations($word) {
        return $this->db->fetchAll("SELECT distinct dialect as dialects, pronunciation as audioFile FROM  words w LEFT JOIN pronunciations p ON p.word = w.id  WHERE name LIKE '%{$word}%' and pronunciation IS NOT NULL    ");
    }

    /**
     * @param string $word
     * @return array
     */
    public function getDefinitions($word) {
        return $this->db->fetchAll("SELECT definition FROM  words w LEFT JOIN definitions d ON d.word = w.id  WHERE name LIKE '%{$word}%'  ");
    }

    /**
     * @param string $word
     * @return array
     */
    public function getNameByFilter($word) {
        return $this->db->fetchAll("SELECT name FROM  words WHERE name LIKE '%{$word}%'");
    }


}
