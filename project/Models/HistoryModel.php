<?php


class HistoryModel
{
    private $db;
    public function __construct()
    {
        $this->db = new DB();
    }

    /**
     * @param string|int $user_id
     * @param string $word
     */

    public function addHistory($user_id, $word){
        $this->db->exec("INSERT INTO history VALUES ({$user_id}, {$word})");
    }

    /**
     * @param string|int $user_id
     * @return array
     */
    public function getWordName($user_id) {
        return $this->db->fetchAll("SELECT name, w.id FROM history h LEFT JOIN words w ON w.id = h.word_id WHERE user_id={$user_id}");
    }

    /**
     * @param string|int $user_id
     * @return array
     */
    public function getWordDefinitions($user_id){
        return $this->db->fetchAll("SELECT definition, d.word as id FROM history h LEFT JOIN definitions d ON d.word = h.word_id WHERE user_id={$user_id}");
    }

    /**
     * @param string|int $user_id
     * @return array
     */
    public function getWordPronunciations($user_id){
        return $this->db->fetchAll("SELECT pronunciation, p.word as id FROM history h LEFT JOIN pronunciations p ON p.word = h.word_id WHERE user_id={$user_id}");
    }
}