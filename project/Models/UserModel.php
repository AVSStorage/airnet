<?php


class UserModel
{
    private $db;
    public function __construct()
    {
        $this->db = new DB();
    }

    /**
     * @param string $login
     * @param string $password
     */

    public function addUser($login, $password) {
        $password = password_hash( $password,PASSWORD_DEFAULT);
        $this->db->exec('INSERT INTO users VALUES (Null, "' . $login . '", "' . $password . '")');
    }


    /**
     * @param string $login
     * @return array
     */
    public function getUserByLogin($login) {
        return $this->db->fetch('SELECT * FROM users WHERE login="' . $login . '"');
    }

    /**
     * @param string $enteredPassword
     * @param string $hashedPassword
     * @return bool
     */
    public function compareUserPassword($enteredPassword, $hashedPassword) {
        return password_verify($enteredPassword, $hashedPassword);
    }
}