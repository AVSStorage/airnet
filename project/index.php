<?php

require_once('Router.php');
require_once('Services/Request.php');
require_once('Controllers/UserController.php');
require_once('Services/APIDataMapper.php');
require_once('Controllers/WordController.php');
require_once('Controllers/HistoryController.php');
session_start();

/**
 *  Route for add custom translation
 */


Router::route('/api/word/add', function(){

    $checkHasData = Router::checkHasPostData(['word','definitions']);

    if ($_SERVER['REQUEST_METHOD'] === 'POST' && $checkHasData) {
        $word =  htmlspecialchars($_POST['word']);
        $definitions = $_POST['definitions'];
        echo  (new WordController())->add($word, json_decode($definitions, true));
        return;
    }

    header('HTTP/1.0 404 Not Found', true, 404);
    return;
});

/**
 *  Route for get translation
 */

Router::route('/api/word/define', function(){


    $checkHasData = Router::checkHasPostData(['word']);
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && $checkHasData) {

        $word = htmlspecialchars($_POST['word']);
        /** Check the word is already exists in DB */
        $checkIsExistsInDB = json_decode((new WordController())->get($word));

        /** Put word to the history */
        (new HistoryController())->add($_SESSION['user_id'], $word);


        if (empty($checkIsExistsInDB)) {
            /** Create request for API */
            $request = new Request();
            $data = $request->getWord($word);

            /** Transform API result into needle data structure */
            $result = (new APIDataMapper($data))->getTransformedData();
            /**  Add data to the DB */
            (new WordController())->add($word, $result['definitions'], $result['pronunciations']);
            echo json_encode($result);
        } else {

            /** Get data from DB */
            echo (new WordController())->getAllExistingData($word);
        }
        return;
    }

    header('HTTP/1.0 404 Not Found', true, 404);
    return;
});

/**
 *  Route for getting history
 */

Router::route('/api/history/get', function(){

    if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_SESSION['user_id'])) {
        echo (new HistoryController())->get($_SESSION['user_id']);
        return;
    }

});

/**
 *  Route for getting autocomplete suggestions
 */

Router::route('/api/word/get/(\w+)', function($word){



    if ($_SERVER['REQUEST_METHOD'] === 'GET') {

        echo  (new WordController())->get($word);
        return;
    }

    header('HTTP/1.0 404 Not Found', true, 404);
    return;
});

/**
 *  Route for login
 */

Router::route('/api/login', function(){

    $checkHasData = Router::checkHasPostData(['login','password']);

    if ($_SERVER['REQUEST_METHOD'] === 'POST' && $checkHasData) {
        $login =  htmlspecialchars($_POST['login']);
        $password = htmlspecialchars($_POST['password']);
        echo  (new UserController())->login($login, $password);
        return;
    }

    header('HTTP/1.0 404 Not Found', true, 404);
    return;
});

/**
 *  Route for registration
 */


Router::route('/api/register', function(){

    $checkHasData = Router::checkHasPostData(['login','password']);
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && $checkHasData) {
        $login =  htmlspecialchars($_POST['login']);
        $password = htmlspecialchars($_POST['password']);
        echo  (new UserController())->register($login, $password);
        return;
    }

    header('HTTP/1.0 404 Not Found', true, 404);
    return;
});






Router::execute($_SERVER['REQUEST_URI']);
?>